<?php

declare(strict_types=1);

namespace App\Handler;

use App\Query\GetVouchers;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

class GetVouchersHandler implements MessageHandlerInterface
{
    public function __invoke(GetVouchers $query): array
    {
        return $query->getUser()->getVouchers()->toArray();
    }
}
