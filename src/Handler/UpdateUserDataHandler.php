<?php

declare(strict_types=1);

namespace App\Handler;

use App\Entity\User;
use App\Query\UpdateUserData;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\Persistence\ObjectManager;
use Exception;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

class UpdateUserDataHandler implements MessageHandlerInterface
{
    private ObjectManager $entityManager;

    public function __construct(ManagerRegistry $doctrine)
    {
        $this->entityManager = $doctrine->getManager();
    }

    public function __invoke(UpdateUserData $query): bool
    {
        /** @var User $user */
        $user = $query->getUser();
        try {
            $user->setFirstname($query->getFirstname());
            $user->setLastname($query->getLastname());
            $user->setPhone($query->getPhone());

            $this->entityManager->persist($user);
            $this->entityManager->flush();
        } catch (Exception $e) {
            return false;
        }

        return true;
    }
}
