<?php

declare(strict_types=1);

namespace App\Handler;

use App\Command\AddVoucher;
use App\Entity\Voucher;
use Doctrine\Persistence\ManagerRegistry;
use Doctrine\Persistence\ObjectManager;
use Exception;
use Ramsey\Uuid\Uuid;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;

class AddVoucherHandler implements MessageHandlerInterface
{
    private ObjectManager $entityManager;

    public function __construct(ManagerRegistry $doctrine, private Voucher $entity)
    {
        $this->entityManager = $doctrine->getManager();
    }

    public function __invoke(AddVoucher $command): bool
    {
        try {
            $this->entity
                ->setId((string) Uuid::uuid4())
                ->setUser($command->getUser())
                ->setType($command->getType());

            $this->entityManager->persist($this->entity);
            $this->entityManager->flush();
        } catch (Exception $e) {
            return false;
        }

        return true;
    }
}
