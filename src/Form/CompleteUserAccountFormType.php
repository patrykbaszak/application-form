<?php

namespace App\Form;

use App\Entity\User;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Validator\Constraints as Assert;

class CompleteUserAccountFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options): void
    {
        $builder
            ->add('firstname', TextType::class, [
                'label' => 'Imię',
            ])
            ->add('lastname', TextType::class, [
                'label' => 'Nazwisko',
            ])
            ->add(
                'phone',
                TextType::class,
                [
                    'label' => 'Telefon',
                    'constraints' => new Assert\Regex('/^\+([0-9]){2}? [0-9]{2,3}? [0-9]{2,3}? [0-9]{2,3}?($|( [0-9]{2,3}))/'),
                ]
            );
    }

    public function configureOptions(OptionsResolver $resolver): void
    {
        $resolver->setDefaults([
            'data_class' => User::class,
        ]);
    }
}
