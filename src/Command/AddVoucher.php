<?php

declare(strict_types=1);

namespace App\Command;

use App\Entity\User;

class AddVoucher
{
    public function __construct(private User $user, private string $type)
    {
    }

    public function getUser(): User
    {
        return $this->user;
    }

    public function getType(): string
    {
        return $this->type;
    }
}
