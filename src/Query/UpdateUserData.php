<?php

declare(strict_types=1);

namespace App\Query;

use App\Entity\User;

class UpdateUserData
{
    public function __construct(
        private User $user,
        private string $firstname,
        private string $lastname,
        private string $phone
    ) {
    }

    public function getUser(): User
    {
        return $this->user;
    }

    public function getFirstname(): string
    {
        return $this->firstname;
    }

    public function getLastname(): string
    {
        return $this->lastname;
    }

    public function getPhone(): string
    {
        return $this->phone;
    }
}
