<?php

declare(strict_types=1);

namespace App\Query;

use App\Entity\User;

class GetVouchers
{
    public function __construct(private User $user)
    {
    }

    public function getUser(): User
    {
        return $this->user;
    }
}
