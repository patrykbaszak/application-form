<?php

declare(strict_types=1);

namespace App\Controller;

use App\Command\AddVoucher;
use App\Entity\User;
use App\Query\GetVouchers;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Messenger\HandleTrait;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\User\UserInterface;

class VoucherController extends AbstractController
{
    use HandleTrait;

    private const TYPE_VOUCHER = 'VOUCHER';
    private const TYPE_GIFTCARD = 'GIFT-CARD';

    public function __construct(MessageBusInterface $messageBus)
    {
        $this->messageBus = $messageBus;
    }

    #[Route('/vouchers', name: 'get_vouchers', methods: ['GET'])]
    public function getVouchers(): Response
    {
        if (!$this->isUserGranted()) {
            return $this->redirectToRoute('index');
        }

        /** @var User $user */
        $user = $this->getUser();

        return $this->render('voucher/getVouchers.html.twig', [
            'vouchers' => $this->handle(new GetVouchers($user)),
            'VOUCHER' => self::TYPE_VOUCHER,
        ]);
    }

    #[Route('/vouchers', name: 'add_voucher', methods: ['POST'])]
    public function addVoucher(): Response
    {
        if (!$this->isUserGranted()) {
            return $this->redirectToRoute('index');
        }

        /** @var User $user */
        $user = $this->getUser();
        $isAdded = $this->handle(new AddVoucher(
            $user,
            self::TYPE_VOUCHER
        ));

        return $this->render('voucher/addVoucher.html.twig', [
            'VOUCHER' => self::TYPE_VOUCHER,
            'type' => self::TYPE_VOUCHER,
            'isAdded' => $isAdded,
        ]);
    }

    #[Route('/gift-card', name: 'add_giftcard', methods: ['POST'])]
    public function addGiftCard(): Response
    {
        if (!$this->isUserGranted()) {
            return $this->redirectToRoute('index');
        }

        /** @var User $user */
        $user = $this->getUser();
        $isAdded = $this->handle(new AddVoucher(
            $user,
            self::TYPE_GIFTCARD
        ));

        return $this->render('voucher/addVoucher.html.twig', [
            'VOUCHER' => self::TYPE_VOUCHER,
            'type' => self::TYPE_GIFTCARD,
            'isAdded' => $isAdded,
        ]);
    }

    private function isUserGranted(): bool
    {
        /** @var User $user */
        $user = $this->getUser();

        if (!$user instanceof UserInterface) {
            return false;
        }

        if (
            null !== $user->getFirstname()
            && null !== $user->getLastname()
            && null !== $user->getPhone()) {
            return true;
        }

        return false;
    }
}
