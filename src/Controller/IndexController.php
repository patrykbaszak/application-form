<?php

namespace App\Controller;

use App\Entity\User;
use App\Form\CompleteUserAccountFormType;
use App\Query\UpdateUserData;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Messenger\HandleTrait;
use Symfony\Component\Messenger\MessageBusInterface;
use Symfony\Component\Routing\Annotation\Route;

class IndexController extends AbstractController
{
    use HandleTrait;

    public function __construct(
        MessageBusInterface $bus
    ) {
        $this->messageBus = $bus;
    }

    #[Route('/', name: 'index')]
    public function index(Request $request): Response
    {
        /** @var User $user */
        $user = $this->getUser();
        if (!$user instanceof User) {
            return $this->render('index/index.html.twig', []);
        }

        $form = $this->createForm(CompleteUserAccountFormType::class);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $this->handle(
                    new UpdateUserData(
                        $user,
                        $form->get('firstname')->getData(),
                        $form->get('lastname')->getData(),
                        $form->get('phone')->getData(),
                    )
                );
        }

        $isComplete = $user->getFirstName() && $user->getLastName() && $user->getPhone() ? true : false;

        return $this->render('index/logedInIndex.html.twig', [
                'username' => $user->getUsername(),
                'isComplete' => $isComplete,
                'form' => $form->createView(),
                'firstname' => $user->getFirstName(),
        ]);
    }
}
