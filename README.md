# Application Form #
Application form for receiving a voucher. The project was created as a test task.

## Local installation ##

### Requirements: ###
- **Git** installed
- **Docker** installed
- **Docker-compose** installed

### Steps: ###
To run Application Form for the first time, go to the directory where you want to put your local repository and execute the following commands:

```bash
> git clone https://gitlab.com/patrykbaszak/application-form.git ApplicationForm
> cd ApplicationForm
> docker-compose up   # do not kill the process, just use another terminal. You can close current terminal.
> docker-compose exec fpm /bin/bash
> composer install
> php bin/console doctrine:migrations:migrate # accept all questions
```

Then go to your browser and open https://app.localhost/ and accept the certificates. The "app" hostname is arbitrary and you can change it.
