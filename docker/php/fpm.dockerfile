FROM php:fpm-alpine

VOLUME [ "/data" ]

RUN mkdir /app \
    && mkdir /app/var \
    && chown -R www-data:www-data /app/var

# update apk repo and install php standard extensions and intl extension
RUN set -xe \
    && apk add --update \
        icu \
    && apk add --no-cache --virtual .php-deps \
        make \
    && apk add --no-cache --virtual .build-deps \
        $PHPIZE_DEPS \
        zlib-dev \
        icu-dev \
        g++ \
    && docker-php-ext-configure intl \
    && docker-php-ext-install \
        intl \
    && docker-php-ext-enable intl \
    && { find /usr/local/lib -type f -print0 | xargs -0r strip --strip-all -p 2>/dev/null || true; } \
    && apk del .build-deps \
    && rm -rf /tmp/* /usr/local/lib/php/doc/* /var/cache/apk/*

# install bash
RUN apk add bash

# install opcache
RUN docker-php-ext-install opcache

# install PDO MySQL library
RUN docker-php-ext-install pdo_mysql

# install composer
RUN php -r "readfile('http://getcomposer.org/installer');" | php -- --install-dir=/usr/bin/ --filename=composer